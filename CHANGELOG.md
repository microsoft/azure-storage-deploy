# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 2.0.1

- patch: Fix usage both source and destination SAS tokens.

## 2.0.0

- major: Update pipe to use AzCopy v10.

## 1.1.2

- patch: Update support link and license to Microsoft.

## 1.1.1

- patch: Update license to MIT.

## 1.1.0

- minor: Updated pipe to use AzCopy 7.3.0.

## 1.0.0

- major: Initial release.

